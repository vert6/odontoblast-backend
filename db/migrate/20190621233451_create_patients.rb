class CreatePatients < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
      t.string :name, null: false
      t.string :gender
      t.date :birthdate
      t.string :address
      t.string :number
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
