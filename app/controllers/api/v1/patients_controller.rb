module Api
  module V1
    class PatientsController < ApplicationController
      #before_action :authorize_access_request!
      #before_action :set_patient, only: [:show, :update, :destroy]

      # GET /patients
      def index
        @patients = current_user.patients.all

        render json: @patients
      end

      # GET /patients/1
      def show
        render json: @patient
      end

      # POST /patients
      def create
        @patient = current_user.patients.build(patient_params)

        if @patient.save
          render json: @patient, status: :created, location: @patient
        else
          render json: @patient.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /patients/1
      def update
        if @patient.update(patient_params)
          render json: @patient
        else
          render json: @patient.errors, status: :unprocessable_entity
        end
      end

      # DELETE /patients/1
      def destroy
        @patient.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_patient
          @patient = current_user.patients.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def patient_params
          params.require(:patient).permit(:name, :gender, :birthdate, :address, :number, :user_id)
        end
    end
  end
end
