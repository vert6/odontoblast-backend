class User < ApplicationRecord
    has_secure_password
    has_many :patients, dependent: :destroy
    validates :first_name, :last_name, :email, :number, :password_digest, presence: true
end
