class Patient < ApplicationRecord
  belongs_to :user
  validates :name, :gender, :birthdate, :number, presence: true
end
