require 'test_helper'

class PatientTest < ActiveSupport::TestCase
  setup do
    @patient = patients(:one)
  end
  
  test "name validation should trigger" do
    assert_not Patient.new(gender: 'Masculino', 
    birthdate: 2019-06-21, address: 'Rua Teste', number: '8888-8888',
    user_id: @patient.user_id).save, "Saved the pacient without a name"
  end

  test "gender validation should trigger" do
    assert_not Patient.new(name: 'Mateus', 
    birthdate: 2019-06-21, address: 'Rua Teste', number: '8888-8888', 
    user_id: @patient.user_id).save, "Saved the pacient without a gender"
  end

  test "birthdate validation should trigger" do
    assert_not Patient.new(name: 'Mateus', 
    gender: 'Masculino', address: 'Rua Teste', number: '8888-8888', 
    user_id: @patient.user_id).save, "Saved the pacient without a birthdate"
  end

  test "number validation should trigger" do
    assert_not Patient.new(name: 'Mateus', 
    gender: 'Masculino', birthdate: 2019-06-21, address: 'Rua Teste', 
    user_id: @patient.user_id).save, "Saved the pacient without a number"
  end

  test "user_id validation should trigger" do
    assert_not Patient.new(name: 'Mateus', 
    gender: 'Masculino',  birthdate: 2019-06-21, address: 'Rua Teste', 
    number: '8888-8888').save, "Saved the pacient without a user_id"
  end

  test "Patient should save" do
    assert Patient.new(gender: 'Masculino', 
    birthdate: 2019-06-21, address: 'Rua Teste', number: '8888-8888',
    user_id: @patient.user_id).save, "The pacient was not saved"
  end
end
