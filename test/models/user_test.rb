require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "first_name validation should trigger" do
    assert_not Patient.new(last_name: 'Teste', 
    email: 'mateus@email.com', number: '8888-8888', 
    password_digest: 'senha').save, "Saved the user without a first_name"
  end

  test "last_name validation should trigger" do
    assert_not Patient.new(first_name: 'Mateus', 
    email: 'mateus@email.com', number: '8888-8888', 
    password_digest: 'senha').save, "Saved the user without a last_name"
  end

  test "email validation should trigger" do
    assert_not Patient.new(first_name: 'Mateus', 
    last_name: 'Teste', number: '8888-8888', 
    password_digest: 'senha').save, "Saved the user without a email"
  end

  test "number validation should trigger" do
    assert_not Patient.new(first_name: 'Mateus', 
    last_name: 'Teste', email: 'mateus@email.com', 
    password_digest: 'senha').save, "Saved the user without a number"
  end

  test "password validation should trigger" do
    assert_not Patient.new(first_name: 'Mateus', 
    last_name: 'Teste', email: 'mateus@email.com')
    .save, "Saved the user without a password"
  end

  test "User should save" do
    assert_not Patient.new(first_name: 'Mateus', 
    last_name: 'Teste', email: 'mateus@email.com', number: '8888-8888', 
    password_digest: 'senha').save, "The user was not saved".
  end
end
