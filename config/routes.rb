Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :appointments
      resources :patients
      resources :users
    end
  end

  root to: "home#index"

  # Authentication routes
  post "refresh", controller: :refresh, action: :create
  post "signup", controller: :signup, action: :create
  post "signin", controller: :signin, action: :create
  delete "signin", controller: :signin, action: :destroy

end
