FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y postgresql-client
RUN mkdir /odontoblast-backend
WORKDIR /odontoblast-backend
COPY Gemfile /odontoblast-backend/Gemfile
COPY Gemfile.lock /odontoblast-backend/Gemfile.lock
RUN bundle install
COPY . /odontoblast-backend

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 5000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
